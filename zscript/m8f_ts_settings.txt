/* Copyright Alexander Kromm (mmaulwurff@gmail.com) 2018
 *
 * This file is part of Target Spy.
 *
 * Target Spy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Target Spy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Target Spy.  If not, see <https://www.gnu.org/licenses/>.
 */

class m8f_ts_Settings
{

  enum FrameStyles
  {
    FRAME_DISABLED,
    FRAME_SLASH,
    FRAME_DOTS,
    FRAME_LESS_GREATER,
    FRAME_GREATER_LESS,
    FRAME_BARS,
    FRAME_GRAPHIC,
  };

  enum BarsOnTargetPosition
  {
    ON_TARGET_DISABLED,
    ON_TARGET_ABOVE,
    ON_TARGET_BELOW,
  }

  int colors[12];

  bool showKillConfirmation;
  bool isEnabled;

  int    minHealth;
  double yStart;
  double yOffset;
  bool   logScale;
  bool   showBar;
  bool   showName;
  int    showNums;
  bool   showInfo;
  bool   showCorps;
  bool   crossOn;
  int    crossCol;
  int    nameCol;
  int    weakCol;
  bool   altHpCols;
  double stepMult;
  bool   almDeadCr;
  int    crAlmDead;
  double crossOff;
  double topOff;
  double botOff;
  int    greenCr;
  int    redCr;
  bool   showChampion;
  int    showObjects;
  int    showInternalNames;
  bool   showHidden;
  bool   showFriends;
  double crossScale;
  bool   hitConfirmation;
  int    hitColor;
  double textScale;
  double xAdjustment;
  bool   noCrossOnSlot1;
  int    frameStyle;
  double frameScale;
  bool   showIdle;
  bool   hideInDarkness;
  int    minimalLightLevel;
  double crossOpacity;
  double opacity;
  double lengthMultiplier;
  int    barsOnTarget;

  string pip;
  string emptyPip;
  string fontName;
  string crosshair;
  string crossTop;
  string crossBot;
  string crossFontName;

  string pipSource;
  string emptyPipSource;

  bool   initialized;

  void read(PlayerInfo player)
  {
    showKillConfirmation = CVar.GetCVar("m8f_ts_show_confirm" , player).GetInt();
    isEnabled            = CVar.GetCVar("m8f_ts_enabled"      , player).GetInt();
    minHealth            = CVar.GetCVar("m8f_ts_min_health"   , player).GetInt();
    yStart               = CVar.GetCVar("m8f_ts_y"            , player).GetFloat();
    yOffset              = CVar.GetCVar("m8f_ts_y_offset"     , player).GetFloat();
    logScale             = CVar.GetCVar("m8f_ts_bar_log_scale", player).GetInt();
    showBar              = CVar.GetCVar("m8f_ts_show_bar"     , player).GetInt();
    showName             = CVar.GetCVar("m8f_ts_show_name"    , player).GetInt();
    showNums             = CVar.GetCVar("m8f_ts_show_numbers" , player).GetInt();
    showInfo             = CVar.GetCVar("m8f_ts_show_info"    , player).GetInt();
    showCorps            = CVar.GetCVar("m8f_ts_show_corpses" , player).GetInt();
    crossOn              = CVar.GetCVar("m8f_ts_crosshair_on" , player).GetInt();
    crossCol             = CVar.GetCVar("m8f_ts_def_color_crs", player).GetInt();
    nameCol              = CVar.GetCVar("m8f_ts_def_color_tag", player).GetInt();
    weakCol              = CVar.GetCVar("m8f_ts_def_cl_tag_wk", player).GetInt();
    altHpCols            = CVar.GetCVar("m8f_ts_alt_hp_color" , player).GetInt();
    crAlmDead            = CVar.GetCVar("m8f_ts_cr_alm_dead"  , player).GetInt();
    stepMult             = CVar.GetCVar("m8f_ts_step_mult"    , player).GetFloat();
    almDeadCr            = CVar.GetCVar("m8f_ts_alm_dead_cr"  , player).GetInt();
    crossOff             = CVar.GetCVar("m8f_ts_cross_offset" , player).GetFloat();
    topOff               = CVar.GetCVar("m8f_ts_top_offset"   , player).GetFloat();
    botOff               = CVar.GetCVar("m8f_ts_bot_offset"   , player).GetFloat();
    greenCr              = CVar.GetCVar("m8f_ts_green_color"  , player).GetInt();
    redCr                = CVar.GetCVar("m8f_ts_red_color"    , player).GetInt();
    showChampion         = CVar.GetCVar("m8f_ts_show_champion", player).GetInt();
    showObjects          = CVar.GetCVar("m8f_ts_show_objects" , player).GetInt();
    showInternalNames    = CVar.GetCVar("m8f_class_as_tag"    , player).GetInt();
    showHidden           = CVar.GetCVar("m8f_ts_show_hidden"  , player).GetInt();
    showFriends          = CVar.GetCVar("m8f_ts_show_friends" , player).GetInt();
    crossScale           = CVar.GetCVar("m8f_ts_cross_scale"  , player).GetFloat();
    hitConfirmation      = CVar.GetCVar("m8f_ts_hit_confirm"  , player).GetInt();
    hitColor             = CVar.GetCVar("m8f_ts_hit_color"    , player).GetInt();
    textScale            = CVar.GetCVar("m8f_ts_text_scale"   , player).GetFloat();
    xAdjustment          = CVar.GetCVar("m8f_ts_x_adjustment" , player).GetFloat();
    noCrossOnSlot1       = CVar.GetCVar("m8f_ts_no_cross_on_1", player).GetInt();
    frameStyle           = CVar.GetCVar("m8f_ts_frame_style"  , player).GetInt();
    frameScale           = CVar.GetCVar("m8f_ts_frame_scale"  , player).GetFloat();
    showIdle             = CVar.GetCVar("m8f_ts_show_idle"    , player).GetInt();
    hideInDarkness       = CVar.GetCVar("m8f_ts_hide_in_dark" , player).GetInt();
    minimalLightLevel    = CVar.GetCVar("m8f_ts_light_level"  , player).GetInt();
    crossOpacity         = CVar.GetCVar("m8f_ts_cr_opacity"   , player).GetFloat();
    opacity              = CVar.GetCVar("m8f_ts_opacity"      , player).GetFloat();
    lengthMultiplier     = CVar.GetCVar("m8f_ts_length_mult"  , player).GetFloat();
    barsOnTarget         = CVar.GetCVar("m8f_ts_on_target"    , player).GetInt();

    pip           = CVar.GetCVar("m8f_ts_pip"          , player).GetString();
    emptyPip      = CVar.GetCVar("m8f_ts_empty_pip"    , player).GetString();
    fontName      = CVar.GetCVar("m8f_ts_font"         , player).GetString();
    crosshair     = CVar.GetCVar("m8f_ts_crosshair"    , player).GetString();
    crossTop      = CVar.GetCVar("m8f_ts_cross_top"    , player).GetString();
    crossBot      = CVar.GetCVar("m8f_ts_cross_bottom" , player).GetString();
    crossFontName = CVar.GetCVar("m8f_ts_cr_font"      , player).GetString();

    colors[ 0] = CVar.GetCVar("m8f_ts_cr_0" , player).GetInt();
    colors[ 1] = CVar.GetCVar("m8f_ts_cr_1" , player).GetInt();
    colors[ 2] = CVar.GetCVar("m8f_ts_cr_2" , player).GetInt();
    colors[ 3] = CVar.GetCVar("m8f_ts_cr_3" , player).GetInt();
    colors[ 4] = CVar.GetCVar("m8f_ts_cr_4" , player).GetInt();
    colors[ 5] = CVar.GetCVar("m8f_ts_cr_5" , player).GetInt();
    colors[ 6] = CVar.GetCVar("m8f_ts_cr_6" , player).GetInt();
    colors[ 7] = CVar.GetCVar("m8f_ts_cr_7" , player).GetInt();
    colors[ 8] = CVar.GetCVar("m8f_ts_cr_8" , player).GetInt();
    colors[ 9] = CVar.GetCVar("m8f_ts_cr_9" , player).GetInt();
    colors[10] = CVar.GetCVar("m8f_ts_cr_10", player).GetInt();
    colors[11] = CVar.GetCVar("m8f_ts_cr_11", player).GetInt();

    pipSource      = m8f_ts_String.MakeRepeating(pip);
    emptyPipSource = m8f_ts_String.MakeRepeating(emptyPip);

    // prevent division by zero
    if (textScale  == 0.0) { textScale  = 1.0; }
    if (crossScale == 0.0) { crossScale = 1.0; }
    if (frameScale == 0.0) { frameScale = 1.0; }
  }

  m8f_ts_Settings Init(PlayerInfo player)
  {
    read(player);
    return self;
  }

} // class m8f_ts_Settings
